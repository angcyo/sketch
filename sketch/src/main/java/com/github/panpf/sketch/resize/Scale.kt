package com.github.panpf.sketch.resize

enum class Scale {
    START_CROP,
    CENTER_CROP,
    END_CROP,
    FILL,
}