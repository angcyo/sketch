package com.github.panpf.sketch.sample

object AssetImages {
    val FORMATS = arrayOf(
        "asset://sample.jpeg",
        "asset://sample.png",
        "asset://sample.webp",
        "asset://sample.bmp",
        "asset://sample.svg",
        "asset://sample.heic",
        "asset://sample_anim.gif",
        "asset://sample_anim.webp",
    )

    val ANIMS = arrayOf(
        "asset://sample_anim.gif",
        "asset://sample_anim.webp",
    )

    val NUMBERS = arrayOf(
//        "asset://number_0.png",
        "asset://number_1.png",
        "asset://number_2.png",
        "asset://number_3.png",
        "asset://number_4.png",
        "asset://number_5.png",
        "asset://number_6.png",
        "asset://number_7.png",
        "asset://number_8.png",
        "asset://number_9.png",
    )

    val HUGES = arrayOf(
        "asset://sample_huge_world.jpg",
        "asset://sample_huge_card.png",
    )

    val LONGS = arrayOf(
        "asset://sample_long_qmsht.jpg",
        "asset://sample_long_comic.jpg",
    )

    val VIDEOS = arrayOf(
        "asset://sample.mp4"
    )
}
